package com.sam.config_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigClientApp {
	/**
	 * 访问： http://localhost:7002/from，
	 * 返回：config-client::: git-dev-1.0
	 * @param args
	 */
    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApp.class, args);
    }

}